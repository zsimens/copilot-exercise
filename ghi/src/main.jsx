import React from 'react'
import ReactDOM from 'react-dom/client'
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

import { AuthProvider } from './authService'
import ShopItemsPage from "./pages/ShopItemsPage";
import AuthPage from "./pages/AuthPage";
import ApparelsPage from "./pages/ApparelsPage";
import AddToShopPage from "./pages/AddToShopPage";
import ApparelFormPage from "./pages/ApparelFormPage";

import './index.css'


const router = createBrowserRouter([
  {
    path: "/",
    element: <AuthPage />,
  },
  {
    path: "/shop-items",
    element: <ShopItemsPage />,
  },
  {
    path: "/apparels",
    element: <ApparelsPage />,
  },
  {
    path: "/add-to-shop-apparel",
    element: <AddToShopPage />,
  },
  {
    path: "/add-new-apparel",
    element: <ApparelFormPage />,
  },
  {
    path: "/apparels/:apparelId",
    element: <ApparelFormPage />,
  },
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <AuthProvider>
      <RouterProvider router={router} />
    </AuthProvider>
  </React.StrictMode>,
)
