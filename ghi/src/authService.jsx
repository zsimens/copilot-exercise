import React, { createContext, useContext, useEffect, useState } from "react";

const baseUrl = import.meta.env.VITE_API_HOST;

export const AuthContext = createContext({
  token: null,
  setToken: () => null,
  user: null,
  setUser: () => null,
  isLoaded: false,
  setIsLoaded: () => null,
});

export const AuthProvider = (props) => {
  const [token, setToken] = useState(null);
  const [user, setUser] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const { children } = props;

  return (
    <AuthContext.Provider
      value={{ token, setToken, user, setUser, isLoaded, setIsLoaded }}
    >
      <TokenNode />
      {children}
    </AuthContext.Provider>
  );
};

export const useAuthContext = () => useContext(AuthContext);

const useAuthService = () => {
  const { token, setToken, user, setUser, isLoaded, setIsLoaded } =
    useAuthContext();

  useEffect(() => {
    const fetchToken = async () => {
      const url = `${baseUrl}/token`;
      const res = await fetch(url, {
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
      });
      if (res.ok) {
        const { token: newToken, user: newUser } = (await res.json()) ?? {};
        setToken(newToken);
        setUser(newUser);
      } else {
        console.error("fetch token failed");
      }
      setIsLoaded(true);
    };

    if (!token) {
      fetchToken();
    }
  }, []);

  const signup = async (userData) => {
    const url = `${baseUrl}/api/employees`;
    const res = await fetch(url, {
      method: "POST",
      body: JSON.stringify(userData),
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (res.ok) {
      const { token: newToken, user: newUser } = (await res.json()) ?? {};
      setToken(newToken);
      setUser(newUser);
      return true;
    } else {
      console.error("sign up failed");
      return false;
    }
  };

  const signin = async (userData) => {
    const url = `${baseUrl}/token`;
    const res = await fetch(url, {
      method: "POST",
      body: JSON.stringify(userData),
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (res.ok) {
      const { token: newToken, user: newUser } = (await res.json()) ?? {};
      setToken(newToken);
      setUser(newUser);
      return true;
    } else {
      console.error("sign in failed");
      return false;
    }
  };

  const signout = async () => {
    const url = `${baseUrl}/token`;
    const res = await fetch(url, {
      method: "DELETE",
      credentials: "include",
    });
    if (!res.ok) {
      console.error("sign out failed, manually deleting cookie");
    }
    setToken(null);
    setUser(null);
  };

  return {
    token,
    signup,
    signin,
    signout,
    user,
    isLoaded,
  };
};

const TokenNode = () => {
  useAuthService();
  return null;
};

export default useAuthService;
