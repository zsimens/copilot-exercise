import React from 'react';
import { useNavigate } from 'react-router-dom';

const ApparelRow = ({ item, getApparels }) => {
    const navigate = useNavigate();

    const handleEditClick = () => {
        navigate(`/apparels/${item.id}`);
    };

    const handleDeleteClick = async () => {
        const url = `${import.meta.env.VITE_API_HOST}/apparels/${item.id}`;
        const response = await fetch(url, {
            method: 'DELETE',
            credentials: 'include',
        });
        if (response.ok) {
            getApparels();
        } else {
            console.error('Failed to delete shop apparel');
        }
    };

    return (
        <tr>
            <td>{item.item}</td>
            <td>{item.description}</td>
            <td>{item.size}</td>
            <td>{item.price}</td>
            <td><button onClick={handleEditClick}>Edit</button></td>
            <td><button onClick={handleDeleteClick}>Delete</button></td>
        </tr>
    );
};

export default ApparelRow;
