from fastapi import APIRouter, HTTPException, Depends
from queries.apparel import ApparelQueries, ApparelOut, ApparelIn


router = APIRouter()

@router.post("/apparels", response_model=ApparelOut)
async def create_apparel_endpoint(
    apparel: ApparelIn,
    queries: ApparelQueries = Depends(),
):
    new_apparel = queries.create_apparel(apparel)
    if new_apparel is None:
        raise HTTPException(status_code=400, detail="Apparel could not be created")
    return new_apparel

@router.get("/apparels", response_model=list[ApparelOut])
async def read_apparels_endpoint(
    queries: ApparelQueries = Depends(),
):
    apparels = queries.get_all()
    return apparels

@router.get("/apparels/{apparel_id}", response_model=ApparelOut)
async def read_apparel_by_id_endpoint(
    apparel_id: int,
    queries: ApparelQueries = Depends(),
):
    apparel = queries.get_by_id(apparel_id)
    if apparel is None:
        raise HTTPException(status_code=404, detail="Apparel not found")
    return apparel

@router.patch("/apparels/{apparel_id}", response_model=ApparelOut)
async def update_apparel_endpoint(
    apparel_id: int,
    apparel: ApparelIn,
    queries: ApparelQueries = Depends(),
):
    updated_apparel = queries.update_apparel(apparel_id, apparel)
    if updated_apparel is None:
        raise HTTPException(status_code=404, detail="Apparel not found")
    return updated_apparel

@router.delete("/apparels/{apparel_id}")
async def delete_apparel_endpoint(
    apparel_id: int,
    queries: ApparelQueries = Depends(),
):
    deleted = queries.delete_apparel(apparel_id)
    if not deleted:
        raise HTTPException(status_code=404, detail="Apparel not found")
    return {"detail": "Apparel deleted"}
